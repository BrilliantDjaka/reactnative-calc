import React ,{Component} from 'react';
import {TextInput,View,StyleSheet,Text,TouchableOpacity} from 'react-native';

class Calc extends Component{
    
    validKeys=[
        '0','1','2','3','4','5','6','7','8','9','+','-','/','*','=','C'
    ]
   
    state = {
        text:'',
        pendingOperation:null,
        firstOperand:null,
       
    }
    
    handleInput = (text) => {
        this.setState({text : text});
    }
    handleButtonInput = (text) => {
    
        let state = {...this.state};
        if(["+","-","*","/"].indexOf(text)>-1){
            let temp = 0;
            if(this.state.pendingOperation != null && this.state.firstOperand !=null){
                temp = this.Calculate();
               
            }    
            else{
                temp = this.state.text;
            }
                state.pendingOperation=text;
                state.text = '';
                state.firstOperand=temp;
                
            console.log(JSON.stringify(this.state))
            return this.setState(state);
    }
        
        else if(text === '='){
            
            state.text=String(this.Calculate());
            state.firstOperand = String(this.Calculate());
            state.pendingOperation = null;
          
            return this.setState(state);
        }
        else if(text == 'C'){
            state.firstOperand = null;
            state.pendingOperation = null;
            state.text = '';
            
            return this.setState(state);
        }
        state.text = state.text+text;
        this.setState(state)

        console.log(JSON.stringify(this.state))
    }
    Calculate = () =>{
        let result = null;
        switch(this.state.pendingOperation){
            
            case'+':    
            result =  Number(this.state.firstOperand)+Number(this.state.text);
            break;

            case'-':
            result = Number(this.state.firstOperand)-Number(this.state.text);
            break;

            case'*':
            result =  Number(this.state.firstOperand)*Number(this.state.text);
            break;

            case'/':
            result = Number(this.state.firstOperand)/Number(this.state.text);
            break;
        }    
          
            this.setState({pendingOperation:null})
            return result;
        
    }

    
 
    render(){
        const styles =StyleSheet.create({
            input:{
                backgroundColor:'grey',
                height:100,
                width:100+"%",
                color:'white',
                fontSize:48,
                textAlign:'right'
            },
            button:{
                flex:1,
                borderWidth:1,
                justifyContent:'center',
                alignItems:'center',
                backgroundColor:'rgb(224,224,224)'  
            },
            row:{
                flex:1,
                flexDirection:'row',
            },
            btnText:{
                fontSize:36,
                
            }
        })
        return(
            
            <View style={{flex:1}}>
                <TextInput
                style={styles.input}
                onChangeText={(text)=>{this.handleInput(text)}}
                value={this.state.text}
                ></TextInput>
                
                <View style={{flex:1,flexDirection:'column'}}>
                {this.validKeys.map((i,index)=>{
                    if(index % 2 != 0 ) return;
                    return(
                    <View key={index} style={styles.row} >
                        <TouchableOpacity style={styles.button} onPress={()=>{this.handleButtonInput(this.validKeys[index])}} >
                            <Text style={styles.btnText} >{this.validKeys[index]}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button} onPress={()=>{this.handleButtonInput(this.validKeys[index+1])}}>
                            <Text style={styles.btnText}>{this.validKeys[index+1]}</Text>
                        </TouchableOpacity>
                    </View>
                    );
                    
                })}
                </View>

            </View>
            
                
       
        );
        

    }
    
}

export default Calc;